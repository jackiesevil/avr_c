## Why do AVR in C?

Doing AVR in C in regards to the purpose of this repo is to give an insight into how microcontrollers
are really programmed. Using the Arduino IDE poses quite a few problems especially regarding the 
overhead involved in uploading problems. 

This is also necessary for the final launch as we may be programming in something other than
an AVR chip, which means that we will need to eventually adjust to using plain C.

## Steps for setting up AVR on mac

**-** Download CrosspackAVR from here: https://www.obdev.at/products/crosspack/index.html
	**-** There are instructions written on that page for compiling the program on an Atmega8 chip
	
> At this point just setting up CrosspackAVR its good to just go in your terminal and type avr-gcc
to make sure that it has linked properly.

**-** Type in _which avr-gcc_ and this should print out a file path under CrosspackAVR somthing like

_/usr/local/CrosspackAVR/bin/avr-gcc_ 

and 

_/usr/local/CrosspackAVR/bin/avrdude_ 

for avr-gcc and avrdude.

Clone the above repository to get started.

Some things you should learn about regarding the make file are the FUSE settings and the different
flags under avrdude which can be viewed by typing _avrdude_ in the terminal.

## Steps for programming AVR on mac

**-** When programming the Arduino there are a few things in the makefile that you will probably need
to change.

> First is the location of the device driver, which on mac should be something like 

_/dev/tty.usb*_ 

or

_/dev/cu.usbmodem*_. 

To list the current drivers type in the following command:

_ls /dev/ | grep "\btty\.usb.*"_

_ls /dev/ | grep "\bcu\.usb.*"_

Something like tty.usbmodem1421 may pop up, replace the _PROGRAMMER = -c arduino -P <over_here> -b 115200_
with _PROGRAMMER = -c arduino -P /dev/tty.usbmodem1421 -b 115200_. Where /dev/tty.usbmodem1421 is
what what listed after the above commands.

All the other settings should be fine for arduinos with the arduino bootloader.

> **If using an arduino mega or some other arduino based chip** You will need to look up the chip
here:
First go into terminal and type: 

_cd /usr/local/CrossPack-AVR/doc/avrdude_.

Then: 

_open ._

You will then be presented with a bunch of html files, open up **index.html**.
Press **5. Programmer Specific Information**
Then Press **5.1 Atmel STK600**
Then _CMD F and find the chip you are looking for_ if it is not listed on that list it isn't supported.

After you have found your chip, replace _device_name_ in _DEVICE = <device_name>_ with the chip
name listed in that web page specific to your microcontroller.

> At this point everything should be setup, at least for arduinos.
Type in 

_make flash_ 

and that should upload the code in **main.c**. In order to compile
more stuff (as in C files that contain custom header info) you will need to modify the 
makefile to accomodate changes.

Please read up on how gcc compiles multiple c files that are used by the same program. And read up
on how makefiles work in general.

A good starting guide is here: 
> For makefiles: https://www.gnu.org/software/make/manual/html_node/Simple-Makefile.html
> For GCC:		 https://phoxis.org/2009/12/01/beginners-guide-to-gcc/
